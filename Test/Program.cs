﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Morris.Model;
namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            GameBoard g = new GameBoard();
            g.AddPiece(PieceEnum.Black, VertexEnum.a7);
            g.AddPiece(PieceEnum.Black, VertexEnum.d7);
            g.MovePiece(VertexEnum.a7, (VertexEnum)9);
            var v=g.CanMove((VertexEnum)9);
            foreach (var item in v)
            {
                Console.WriteLine(item);   
            }
        }
    }
}
