﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Morris.Model
{
    public enum Player
    {
        Black, White
    }
    public enum GamePhase
    {
        Phase1, //placing pieces
        Phase2, ///moving pieces
        Phase3  ///"flying pieces"
    }
    interface IGameLogic
    {
        bool PutPiece(PieceEnum p, int row, string col);
        bool MovePiece(int sRow, string sCol, int dRow, string dCol);
        bool RemovePiece(int row, string col);
        
    }
}
