﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Morris.Model
{
    public enum PieceEnum
    {
        Black=1, White=-1, Empty=0
    }
    public enum VertexEnum
    {
        a7=0,    d7,      g7,
           b6,   d6,   f6,
              c5,d5,e5,
        a4,b4,c4,   e4,f4,g4,
              c3,d3,e3,
           b2,  d2,    f2,
        a1,     d1,       g1
    }

    interface IGameBoard
    {
        void ClearBoard();

        bool AddPiece(PieceEnum p, VertexEnum pos);
        bool RemovePiece(VertexEnum pos);
        bool MovePiece(VertexEnum pos1, VertexEnum pos2);
        bool MovePieceFree(VertexEnum pos1, VertexEnum pos2);
        List<VertexEnum> CanMove(VertexEnum pos);
        PieceEnum this[VertexEnum naturalIndex]
        {
            get;
            set;
        }
    }
}
