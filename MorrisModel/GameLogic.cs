﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Morris.Model
{
    class GameLogic : IGameLogic
    {
        private int WhiteLeft;
        private int WhiteOnBoard;
        private int BlackLeft;
        private int BlackOnBoard;
        IGameBoard gameBoard=new GameBoard();

        public GameLogic()
        {
            WhiteLeft=9;
            WhiteOnBoard=0;
            BlackLeft=9;
            BlackOnBoard=0;
        }
        public bool PutPiece(PieceEnum p, int row, string col)
        {
            try
            {
                if (p == PieceEnum.Black && BlackLeft > 0)
                {
                    gameBoard[col, row] = p;
                    BlackOnBoard++;
                    BlackLeft--;
                    return true;
                }
                else if (p == PieceEnum.White && WhiteLeft > 0)
                {
                    gameBoard[col, row] = p;
                    WhiteOnBoard++;
                    WhiteLeft--;
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool MovePiece(int sRow, string sCol, int dRow, string dCol)
        {
            try
            {
                if (gameBoard[sCol, sRow] != PieceEnum.Empty && gameBoard[dCol, dRow] == PieceEnum.Empty)
                {
                    PieceEnum p = gameBoard[sCol, sRow];
                    gameBoard[sCol, sRow] = PieceEnum.Empty;
                    gameBoard[dCol, dRow] = p;
                    return true;
                }
                return false;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool RemovePiece(int row, string col)
        {
            try
            {
                gameBoard[col, row] = PieceEnum.Empty;
                return true;
            }
            catch (Exception e)
            {
                return false;
                throw e;
            }
        }
    }
}
