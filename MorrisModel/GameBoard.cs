﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Morris.Model
{
    public class GameBoard : IGameBoard
    {

        public Vertex[] vertecies = new Vertex[24];
        public Edge[] edges = new Edge[32];
        public Mill[] possibleMills = new Mill[16];


        public GameBoard()
        {
            /*vertecies = new Vertex[24];
            edges = new Edge[32];
            possibleMills = new Mill[16];*/
            arrayInit();
        }
        private void arrayInit()
        {
            
            for (int i = 0; i < 24; i++)
            {
                vertecies[i] = new Vertex();
                vertecies[i].pos = (VertexEnum)i;
                vertecies[i].piece = PieceEnum.Empty;
            }
            for (int i = 0; i < 32; i++)
                edges[i] = new Edge();
            for (int i = 0; i < 16; i++)
                possibleMills[i] = new Mill();
            edges[0].v1 = (VertexEnum)0;
            edges[0].v2 = (VertexEnum)1;

            edges[1].v1 = (VertexEnum)1;
            edges[1].v2 = (VertexEnum)2;

            edges[2].v1 = (VertexEnum)0;
            edges[2].v2 = (VertexEnum)9;

            edges[3].v1 = (VertexEnum)1;
            edges[3].v2 = (VertexEnum)4;

            edges[4].v1 = (VertexEnum)2;
            edges[4].v2 = (VertexEnum)14;

            edges[5].v1 = (VertexEnum)3;
            edges[5].v2 = (VertexEnum)4;

            edges[6].v1 = (VertexEnum)4;
            edges[6].v2 = (VertexEnum)5;

            edges[7].v1 = (VertexEnum)3;
            edges[7].v2 = (VertexEnum)10;

            edges[8].v1 = (VertexEnum)4;
            edges[8].v2 = (VertexEnum)7;

            edges[9].v1 = (VertexEnum)5;
            edges[9].v2 = (VertexEnum)19;

            edges[10].v1 = (VertexEnum)6;
            edges[10].v2 = (VertexEnum)11;

            edges[11].v1 = (VertexEnum)6;
            edges[11].v2 = (VertexEnum)7;

            edges[12].v1 = (VertexEnum)7;
            edges[12].v2 = (VertexEnum)8;

            edges[13].v1 = (VertexEnum)8;
            edges[13].v2 = (VertexEnum)12;

            edges[14].v1 = (VertexEnum)9;
            edges[14].v2 = (VertexEnum)10;

            edges[15].v1 = (VertexEnum)10;
            edges[15].v2 = (VertexEnum)11;

            edges[16].v1 = (VertexEnum)12;
            edges[16].v2 = (VertexEnum)13;

            edges[17].v1 = (VertexEnum)13;
            edges[17].v2 = (VertexEnum)14;

            edges[18].v1 = (VertexEnum)11;
            edges[18].v2 = (VertexEnum)15;

            edges[19].v1 = (VertexEnum)15;
            edges[19].v2 = (VertexEnum)16;

            edges[20].v1 = (VertexEnum)16;
            edges[20].v2 = (VertexEnum)17;

            edges[21].v1 = (VertexEnum)17;
            edges[21].v2 = (VertexEnum)18;

            edges[22].v1 = (VertexEnum)10;
            edges[22].v2 = (VertexEnum)18;

            edges[23].v1 = (VertexEnum)18;
            edges[23].v2 = (VertexEnum)19;

            edges[24].v1 = (VertexEnum)16;
            edges[24].v2 = (VertexEnum)19;

            edges[25].v1 = (VertexEnum)19;
            edges[25].v2 = (VertexEnum)20;

            edges[26].v1 = (VertexEnum)13;
            edges[26].v2 = (VertexEnum)20;

            edges[27].v1 = (VertexEnum)9;
            edges[27].v2 = (VertexEnum)21;

            edges[28].v1 = (VertexEnum)19;
            edges[28].v2 = (VertexEnum)22;

            edges[29].v1 = (VertexEnum)14;
            edges[29].v2 = (VertexEnum)23;

            edges[30].v1 = (VertexEnum)21;
            edges[30].v2 = (VertexEnum)22;

            edges[31].v1 = (VertexEnum)22;
            edges[31].v2 = (VertexEnum)23;

            possibleMills[0].v1 = (VertexEnum)0;
            possibleMills[0].v2 = (VertexEnum)1;
            possibleMills[0].v3 = (VertexEnum)2;

            possibleMills[0].v1 = (VertexEnum)0;
            possibleMills[0].v2 = (VertexEnum)9;
            possibleMills[0].v3 = (VertexEnum)21;

            possibleMills[0].v1 = (VertexEnum)21;
            possibleMills[0].v2 = (VertexEnum)22;
            possibleMills[0].v3 = (VertexEnum)23;

            possibleMills[0].v1 = (VertexEnum)23;
            possibleMills[0].v2 = (VertexEnum)14;
            possibleMills[0].v3 = (VertexEnum)2;

            possibleMills[0].v1 = (VertexEnum)3;
            possibleMills[0].v2 = (VertexEnum)4;
            possibleMills[0].v3 = (VertexEnum)5;

            possibleMills[0].v1 = (VertexEnum)3;
            possibleMills[0].v2 = (VertexEnum)10;
            possibleMills[0].v3 = (VertexEnum)18;

            possibleMills[0].v1 = (VertexEnum)18;
            possibleMills[0].v2 = (VertexEnum)19;
            possibleMills[0].v3 = (VertexEnum)20;

            possibleMills[0].v1 = (VertexEnum)5;
            possibleMills[0].v2 = (VertexEnum)13;
            possibleMills[0].v3 = (VertexEnum)20;

            possibleMills[0].v1 = (VertexEnum)6;
            possibleMills[0].v2 = (VertexEnum)7;
            possibleMills[0].v3 = (VertexEnum)8;

            possibleMills[0].v1 = (VertexEnum)6;
            possibleMills[0].v2 = (VertexEnum)11;
            possibleMills[0].v3 = (VertexEnum)15;

            possibleMills[0].v1 = (VertexEnum)15;
            possibleMills[0].v2 = (VertexEnum)16;
            possibleMills[0].v3 = (VertexEnum)17;

            possibleMills[0].v1 = (VertexEnum)8;
            possibleMills[0].v2 = (VertexEnum)12;
            possibleMills[0].v3 = (VertexEnum)17;

            possibleMills[0].v1 = (VertexEnum)1;
            possibleMills[0].v2 = (VertexEnum)4;
            possibleMills[0].v3 = (VertexEnum)7;

            possibleMills[0].v1 = (VertexEnum)9;
            possibleMills[0].v2 = (VertexEnum)10;
            possibleMills[0].v3 = (VertexEnum)11;

            possibleMills[0].v1 = (VertexEnum)16;
            possibleMills[0].v2 = (VertexEnum)19;
            possibleMills[0].v3 = (VertexEnum)22;

            possibleMills[0].v1 = (VertexEnum)12;
            possibleMills[0].v2 = (VertexEnum)13;
            possibleMills[0].v3 = (VertexEnum)14;
        }

        public void ClearBoard()
        {
            for (int i = 0; i < 24; i++)
            {
                vertecies[i].pos = (VertexEnum)i;
                vertecies[i].piece = PieceEnum.Empty;
            }
        }

        public PieceEnum this[VertexEnum naturalIndex]
        {
            get
            {
                return vertecies[(int)naturalIndex].piece;
            }
            set
            {
                vertecies[(int)naturalIndex].piece = value;
            }
        }
        public bool AddPiece(PieceEnum p, VertexEnum pos)
        {
            if (vertecies[(int)pos].piece == PieceEnum.Empty)
            {
                vertecies[(int)pos].piece = p;
                return true;
            }
            return false;
        }
        public bool RemovePiece(VertexEnum pos)
        {
            if (vertecies[(int)pos].piece != PieceEnum.Empty)
            {
                vertecies[(int)pos].piece = PieceEnum.Empty;
                return true;
            }
            return false;
        }
        public bool MovePiece(VertexEnum pos1, VertexEnum pos2)
        {
            if (vertecies[(int)pos1].piece != PieceEnum.Empty && vertecies[(int)pos2].piece == PieceEnum.Empty)
            {
                for (int i = 0; i < 32; i++)
                {
                    if (edges[i].v1 == pos1 && edges[i].v2 == pos2 || edges[i].v1 == pos2 && edges[i].v2 == pos1)
                    {
                        vertecies[(int)pos2].piece = vertecies[(int)pos1].piece;
                        vertecies[(int)pos1].piece = PieceEnum.Empty;
                        return true;
                    }
                }
            }
            return false;
        }
        public bool MovePieceFree(VertexEnum pos1, VertexEnum pos2)
        {
            if (vertecies[(int)pos1].piece != PieceEnum.Empty && vertecies[(int)pos2].piece == PieceEnum.Empty)
            {
                vertecies[(int)pos2].piece = vertecies[(int)pos1].piece;
                vertecies[(int)pos1].piece = PieceEnum.Empty;
                return true;
            }
            return false;
        }
        public List<VertexEnum> CanMove(VertexEnum pos)
        {
           List< VertexEnum> v=new List<VertexEnum>();
            for (int i = 0; i < 32; i++)
            {
                if (edges[i].v1 == pos)
                {
                    v.Add(edges[i].v2);
                }
                else if (edges[i].v2 == pos)
                {
                    v.Add(edges[i].v1);
                }
            }
            return v;
        }
    }
}
